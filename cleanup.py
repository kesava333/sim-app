import pathlib

def zipCleanup(pathToCleanup):
    dir = pathlib.Path(pathToCleanup)
    zip_files = dir.rglob("*.zip")
    for zf in zip_files:
        zf.unlink()