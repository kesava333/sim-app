Mongo Installation
------------------

```
wget https://repo.mongodb.org/apt/ubuntu/dists/bionic/mongodb-org/6.0/multiverse/binary-amd64/mongodb-org-server_6.0.5_amd64.deb

dpkg -i mongodb-org-server_6.0.5_amd64.deb

service mongod start

service mongod status 
```
Mongosh installation
```
wget https://downloads.mongodb.com/compass/mongodb-mongosh_1.10.1_amd64.deb

dpkg -i mongodb-mongosh_1.10.1_amd64.deb

mongosh

```
Create an account

```
db.createUser(
  {
    user: "mongoadmin",
    pwd: "secret",
    roles: [ { role: "readWrite", db: "webapp" } ],
    mechanisms: [ "SCRAM-SHA-1" ]
  }
)

```
