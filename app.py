from flask import Flask, render_template, request, send_file, redirect, url_for
import hashlib
import zipfile
import os
import yaml
from yaml.loader import SafeLoader
import subprocess
from flask_login import UserMixin, login_user, LoginManager, login_required, logout_user
from flask import session, jsonify, Blueprint, flash
from werkzeug.exceptions import Unauthorized
from bson import ObjectId
import shutil
from db import insertDBRecords, findDBRecords, deleteDBDoc, findAllDBRecords, updateDBDoc
from emails import sendEmail
import random
from cleanup import zipCleanup
from aws import terminateInstance
import requests


app = Flask(__name__)
app_bp = Blueprint('auth', __name__)

configFile = "awsfleet-config.yaml"
aws_file = "simstart.yaml"
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'
DownloadZipFileList = ['awsfleet-config.yaml', 'simfleet.log', 'simstart.yaml']
ClusterCreationScript = 'script.sh'
# Log filename to monitor the logs
ClusterOutputLog = 'simfleet.log'
projectDir='projects'
execscript='cluster_exec.sh'


login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

class User(UserMixin):
    def __init__(self, id):
        self.id = id

@login_manager.user_loader
def load_user(user_id):
    return User(user_id)

@app.route("/", methods=['GET', 'POST'])
def home():
    if '_user_id' in session:
        return redirect(url_for('project'))
    return render_template('login.html')

@app.route("/login", methods=['GET', 'POST'])
def login():
    if '_user_id' in session:
        return redirect(url_for('project'))
    else:
        return render_template('login.html')

@app.route("/create", methods=['GET', 'POST'])
@login_required
def createProject():
    return render_template('createProject.html')

@login_required
def getUser(sessionId):
    sessionData = {'_id': ObjectId(sessionId)}
    username = findDBRecords('user',sessionData)['username']
    return username

@app.route('/submit_form', methods=['POST'])
@login_required
def submit_form():
    global filenames, projectName
    projectName = request.form['Project']
    filenames = request.form['Project']
    username = getUser(session['_user_id'])
    checkList = ['num_rocksteady','num_rzr','num_striker', 'num_moon', 'num_starling', 'dev_platform']
    configData = {
        'fleet': {
            'username': username,
            'version': '{:.2f}'.format(float(request.form['Version'])),
            'project': request.form['Project'],
            'budget': request.form['Budget'],
            'dev_platform': request.form['Platform'],
            'num_robots': int(request.form['RobotCount']),
            'num_rocksteady': int(request.form['Rocksteady']),
            'num_rzr': int(request.form['RZR']),
            'num_striker': int(request.form['Striker']),
            'num_moon': int(request.form['Moon']),
            'num_starling': int(request.form['Starling'])
        }
    }
    
    keys_to_delete = []
    for criteria in checkList:
        if configData['fleet'][criteria] == 0 or configData['fleet'][criteria] == '':
            keys_to_delete.append(criteria)
            
    for key in keys_to_delete:
        del(configData['fleet'][key])

    
    os.makedirs(projectDir+'/'+request.form['Project'], mode=0o777, exist_ok=False)
    
    insertDBRecords('project',configData)
    del (configData['fleet']['username'])
    if configData['_id']:
        del (configData['_id'])

    with open(projectDir+'/'+request.form['Project'] + '/' + configFile, 'w') as cf:
        yaml.dump(configData, cf, sort_keys=False)
    return render_template('success.html')

@app.route('/downloadyaml')
@login_required
def downloadYaml():
    projectName = request.args.get('projectName')
    if os.path.exists(projectDir+'/'+projectName+'/'+aws_file):
        return send_file(projectDir+'/'+projectName+"/"+aws_file, as_attachment=True)
    else:
        print(aws_file + " YAML File not found")
        return render_template("project.html", error="simstart yaml not created yet, Please stand by!.")

@app.route('/downloadprojectdata')
@login_required
def downloadprojectdata():
    projectName = request.args.get('projectName')
    print(projectName)
    zip_filename = projectName + '.zip'
    with zipfile.ZipFile(zip_filename, 'w', zipfile.ZIP_DEFLATED) as zipf:
        for root, dirs, files in os.walk(projectDir+'/'+projectName):
            for file in files:
                if file in DownloadZipFileList:    
                    zipf.write(os.path.join(root, file))
    return send_file(zip_filename, as_attachment=True)


@app.route('/download_robot_data')
@login_required
def download_robot_data():
    print(filenames)
    zip_filename = projectName + '.zip'
    with zipfile.ZipFile(zip_filename, 'w', zipfile.ZIP_DEFLATED) as zipf:
        for root, dirs, files in os.walk(projectDir+'/'+filenames):
            for file in files:
                zipf.write(os.path.join(root, file))
    return send_file(zip_filename, as_attachment=True)


@app.route('/signup')
def signup():
    if '_user_id' in session:
        return redirect(url_for('project'))
    return render_template('signup.html')

@app.route('/project')
@login_required
def project():
    flash('You were successfully logged in')
    dictval = []
    username = getUser(session['_user_id'])
    userSessiondata = {'fleet.username': username}
    zipCleanup('./')
    projectData = findAllDBRecords('project',userSessiondata)
    dictval = [{value[0]: value[1]} for data in projectData for value in data['fleet'].items() if 'project' in value]
    projectData.close()
    return render_template('project.html', projects = dictval)

@app.route('/result', methods=['POST'])
def result():
    hashedpassword = hashlib.sha256(request.form['password'].encode('utf-8')).hexdigest()
    data = {'username': request.form['username'], 'password': hashedpassword, 'email': request.form['email']}
    usernameQuery = {'username': request.form['username']}
    emailQuery = {'email': request.form['email']}
    userQueryData = findDBRecords('user', usernameQuery)
    emailQueryData = findDBRecords('user', emailQuery)
    if userQueryData:
        error = 'User already exists. Please try again with a different username.'
        return render_template('signup.html', error=error)
    elif emailQueryData:
         error = 'Email already exists. Please try again with a different email.'
         return render_template('signup.html', error=error)
    else:
        insertDBRecords('user',data)
        print("New user: " + request.form['username'] + " is created")
        return render_template('login.html')


@app.route('/submit', methods=['GET','POST'])
def submit():
    print("User Submitted login")
    if request.method == 'GET':
        return redirect(url_for('project'))
    hashedpassword = hashlib.sha256(request.form['password'].encode('utf-8')).hexdigest()
    data = {'username': request.form['username'], 'password': hashedpassword}
    userData = findDBRecords('user',data)
    if userData:
        user_id = userData['_id']
        user = User(user_id)
        login_user(user)
        print("Login Approved for user " + request.form['username'])
        return redirect(url_for('project'))
    else:
        print("Login rejected for user "+ request.form['username'])
        return render_template('loginfailure.html')

@app.route('/logout')
def logout():
    logout_user()
    zipCleanup('./')
    return render_template('login.html')

@login_manager.unauthorized_handler
def unauthorized():
    return render_template('unauthorized.html')

@app.route('/protected')
@login_required
def protected():
    return render_template('unauthorized.html')

@app.errorhandler(Unauthorized)
def handle_unauthorized(e):
    return redirect(url_for('login'))

@app.errorhandler(404)
def page_not_found(error):
    return render_template('404.html'), 404


@app.route('/deleteproject')
@login_required
def deleteProject():
    projectName = request.args.get('projectName')
    username = getUser(session['_user_id'])
    DeleteDBProjectData = {'$and': [{'fleet.username': username }, {'fleet.project': projectName}]}
    DeleteDBClusterData = {'$and': [{'username': username }, {'project': projectName}, {'clusterStatus': 'Active'}]}
    print(DeleteDBClusterData)
    deleteDBDoc('project',DeleteDBProjectData)
    deleteDBDoc('cluster',DeleteDBClusterData)
    zipCleanup('./')
    instanceList=[]
    if os.path.exists(projectDir+'/'+projectName):
        if os.path.exists(projectDir+'/'+projectName+'/'+aws_file):
            with open(projectDir+'/'+projectName+'/'+aws_file) as f:
                data = yaml.load(f, Loader=SafeLoader)
                for robotType in data.values():
                    try:
                        instanceList.append(robotType['aws'])
                    except Exception as e:
                        for robots in robotType:
                            instanceList.append(robots['aws'])
        terminateInstance(instanceList)
        shutil.rmtree(projectDir+'/'+projectName)
        print(f"The directory {projectName} has been deleted")
    else:
        print(f"The directory {projectName} does not exist")
    return jsonify({'message': 'Project deleted successfully'})

@app.route('/createCluster')
@login_required
def createCluster():
    projectName = request.args.get('projectName')
    username = getUser(session['_user_id'])
    clusterData = {'username': username, 'project': projectName, 'clusterStatus': 'Active'}
    getClusterStatus = findDBRecords('cluster', {'username': username, 'project': projectName})
    if getClusterStatus and getClusterStatus['clusterStatus'] == 'Active':
        return jsonify({'cluster': getClusterStatus['clusterStatus']})
    else:
        clusterDBUpdate = insertDBRecords('cluster', clusterData)
        if clusterDBUpdate:
            return jsonify({'status': 'success', 'message': 'Cluster creation initiated successfully.'})
        else:
            return jsonify({'status': 'fail', 'message': 'Cluster creation failed.'})

@app.route('/getClusterStatus')
@login_required
def getClusterStatus():
    projectName = request.args.get('projectName')
    username = getUser(session['_user_id'])
    getClusterStatus = findDBRecords('cluster', {'username': username, 'project': projectName})
    print(getClusterStatus)
    if getClusterStatus is not None and getClusterStatus['clusterStatus'] == 'Active':
        print("The cluster is Active")
        getClusterStatus['_id'] = str(getClusterStatus['_id'])
        return jsonify({'cluster': getClusterStatus['clusterStatus']})
    else:
        return jsonify({'cluster': 'InActive'})
    
@app.route('/executeShell')
@login_required
def executeShell():
    projectName = request.args.get('projectName')
    Script = request.args.get('scriptName')
    processToken=request.args.get('token')
    print(Script)
    if processToken == 'create':
        subprocess.call(['sh', 'Scripts/'+Script, projectName, projectDir, ClusterOutputLog])
    else:
        print("UPDATE IN PROGRESS")
        subprocess.call(['sh', 'Scripts/'+execscript, projectName, projectDir, Script, ClusterOutputLog])
    return jsonify({'clusterexecution': 'Triggered'})

@app.route('/currentClusterStatus')
@login_required
def currentClusterStatus():
    project_name = request.args.get('projectName')
    log_file_path = './' + projectDir+'/'+project_name + '/'+ ClusterOutputLog
    if os.path.exists(log_file_path):
        print("Log file found")
        log_content = []
        with open(log_file_path) as f:
            for line in f:
                log_content.append(line.strip())
    else:
        log_content = ["No Logs found"]
    return render_template('clusterOutput.html', project_name=project_name, CurrentClusterlogs=log_content)

@app.route('/reset',methods=['GET', 'POST'])
def passReset():
    return render_template("passreset.html")
@app.route('/token',methods=['GET', 'POST'])
def token():
    random_number = random.randint(10000, 99999)
    usernameQuery = {'username': request.form['username']}
    userQueryData = findDBRecords('user', usernameQuery)
    if userQueryData is not None:
        if 'username' in userQueryData:
            tokenQuery = { "$set": { "token": random_number }, "$setOnInsert": { "username": request.form['username'], "email": userQueryData['email']}}
            hashedpassword = hashlib.sha256(request.form['password'].encode('utf-8')).hexdigest()
            resetPassQuery = { "$set": { "password": hashedpassword }, "$setOnInsert": { "username": request.form['username'], "email": userQueryData['email']}}
            updateDBDoc('token', usernameQuery,tokenQuery, True)
            updateDBDoc('userreset', usernameQuery,resetPassQuery, True)
            email_sent = sendEmail(userQueryData['email'],request.form['username'], str(random_number))
            if email_sent:
                return render_template("token.html", success="Token sent successfully to your registered Email", username = request.form['username'], email = userQueryData['email'])
            else:
                return render_template("token.html", error="Failed to send email. Please try again later.", username = request.form['username'], email = userQueryData['email'])
        else:
            return render_template("passreset.html", error="User not found.")
    else:
        return render_template("passreset.html", error="User not found.")
    
@app.route('/tokenvalidate',methods=['GET', 'POST'])
def tokenvalidate():
    usernameQuery = {'username': request.args.get('username'),'email': request.args.get('email')}
    getTokenDetails = findDBRecords('token', usernameQuery)
    getTempPassDetails = findDBRecords('userreset', usernameQuery)
    resetPassQuery = { "$set": { "password": getTempPassDetails['password'] }, "$setOnInsert": { "username": request.args.get('username'), "email": request.args.get('email')}}
    if getTokenDetails['token'] == int(request.form['token']):
        print("Token matched")
        updateDBDoc('user', usernameQuery,resetPassQuery, True)
        deleteDBDoc('userreset',usernameQuery)
        return render_template("login.html", success= "Password changed succesfully, Please Login")
    else:
        return render_template("passreset.html", error="Token Error, Please try again.")

@app.route('/connect', methods=['GET', 'POST'])
def connect():
    auth = {'webapp','mani@12345'}
    response = requests.get(url="http://127.0.0.1:5002/login", auth=auth)
    #response = requests.post(url="http://127.0.0.1:5002/add_system")
    print(response)

if __name__ == '__main__':
    app.run(host='10.31.15.35', port=5000)
    #app.run("127.0.0.1", port=5000)
    app.run(debug=True)
