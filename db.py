from pymongo import MongoClient

#DBConnString = 'mongodb://mongoadmin:secret@localhost:27888/?authSource=admin'
DBConnString = 'mongodb://localhost:27017'
DBName = 'webapp'

def insertDBRecords(collectionName,DataToInsert):
    client = MongoClient(DBConnString)
    db = client[DBName]
    collection = db[collectionName]
    collection.insert_one(DataToInsert)
    client.close()

def findDBRecords(collectionName,DataToFind):
    client = MongoClient(DBConnString)
    db = client[DBName]
    collection = db[collectionName]
    dataCollected = collection.find_one(DataToFind)
    client.close()
    return dataCollected

def findAllDBRecords(collectionName,DataToFind):
    client = MongoClient(DBConnString)
    db = client[DBName]
    collection = db[collectionName]
    dataCollected = collection.find(DataToFind)
    return dataCollected

def deleteDBDoc(collectionName,DataToDelete):
    client = MongoClient(DBConnString)
    db = client[DBName]
    collection = db[collectionName]
    delResponse = collection.delete_one(DataToDelete)
    client.close()
    return delResponse

def updateDBDoc(collectionName,DataToQuery,DataToUpdate,UpsertStatus):
    client = MongoClient(DBConnString)
    db = client[DBName]
    collection = db[collectionName]
    updateResponse = collection.update_one(DataToQuery, DataToUpdate, upsert=UpsertStatus)
    client.close()
    return updateResponse