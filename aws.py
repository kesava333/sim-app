import boto3

ec2Client = boto3.client('ec2')
regionsList = ['us-east-2']

def terminateInstance(instanceTerminateList):
    for region in regionsList:
        ec2 = boto3.resource('ec2', region_name=region)
        for instanceId in instanceTerminateList:
            instance = ec2.Instance(instanceId)
            print(instanceId + ' terminated')
            instance.stop(DryRun=False)
