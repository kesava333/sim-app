#!/bin/bash
echo "AWS Simulation Creation Initiated for $1"
cp -r Scripts/*  $2/$1/
cd $2/$1
exec >> "$3" 2>&1
./deploy
